//Program Stack Dengan Array Dinamis
#include<iostream>
using namespace std;

class Stack{
	friend ostream& operator<<(ostream&, const Stack&);
	public:
			Stack();
			int penuh(int x);
			int kosong(int x);
			void cetak();
			void push(char y);

			char pop();
			int banyakElemen;
	private:
			char *A;
			int banyak;

};

ostream& operator <<(ostream& out,const Stack& s){
		cout<<"\nCetak Stack Dengan Overloading : ";
		for(int i=0;i<s.banyak;i++)
		out<<s.A[i]<<" ";
}

Stack::Stack(){
		cout<<"Masukan Banyak Elemen : ";
		cin>>banyakElemen;
		cout<<endl;
		A = new char[banyakElemen];
		banyak=0;
		for(int i=0;i<banyakElemen;i++)
		A[i] = '0';
}

int Stack::penuh(int s){
		return s == banyakElemen ? 1 : 0;
}

void Stack::cetak(){
		cout<<"Isi Stack  : ";
		for(int i=0;i<banyak;i++){
		cout<<A[i]<<" ";
		}
		cout<<endl;
}

void Stack::push(char x){
		if(penuh(banyak)) cout<<"WARNING! Stack Penuh"<<endl;
		else if(A[0] == '0'){
	
		A[0] = x;
		banyak++;
}else{
		for(int i=banyak;i>=0;i--)
		A[i+1] = A[i];
		A[0] = x;
		banyak++;
	}
}

char Stack::pop(){
		cout<<"Elemen Yang Di Pop : "<<A[0]<<endl;
		char temp=A[0];
		for(int i=0;i<banyak;i++) A[i] = A[i+1];
		A[banyak] = '0';
		banyak--;
		return temp;
}
int main(){
		Stack stack;
		char j,isilagi;
		for(int i=0;i<stack.banyakElemen;i++){
		cout<<"Masukan Elemen Ke "<<(i+1)<<" : ";
		cin>>j;
		stack.push(j);
	}
		cout<<endl;
		stack.cetak();
		char p = stack.pop();
		stack.cetak();
		cout<<stack;
}
