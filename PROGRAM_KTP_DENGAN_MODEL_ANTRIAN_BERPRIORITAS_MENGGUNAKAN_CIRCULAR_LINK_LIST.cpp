//PROGRAM KTP DENGAN MODEL ANTRIAN BERPRIORITAS MENGGUNAKAN CIRCULAR LINK LIST
#include<iostream>

using namespace std;

struct node {
	string nama;
	string ttl;
	string jenis_kelamin; 
	string alamat;
	string rt_rw; 
	string agama;
	string status_perkawinan; 
	string pekerjaan;
	string kewarganegaraan; 
	
	node *next;
	node *prev;

}

*front=NULL,*rear=NULL,*n,*temp,*temp1;

class circular_list {

public:

	void tambah_data(); void hapus_data(); void tampilkan_list();
};

int main() {
	circular_list queue; 
	char ch;
do {

system("cls");

cout<<"PROGRAM ANTRIAN BERPRIORITAS DENGAN LINK LIST"<<endl;
cout<<"1. Tambah Data(enqueue) "<<endl;
cout<<"2. Hapus Data(dequeue)"<<endl;
cout<<"3. Lihat Data"<<endl;
cout<<"4. Exit "<<endl;
cout<<"Masukan Pilian: ";
cin>>ch;
cin.ignore(); 

switch(ch) {

case '1': 
	queue.tambah_data(); 
	system("pause");
	break; 

case '2':
	queue.hapus_data(); 
	cout<<endl; 
	system("pause"); 
	break;
	
case '3': 
	queue.tampilkan_list(); 
	cout<<endl; 
	system("pause"); 
	break;
	
case '4':
	exit(1); 
	break; 

default:

cout<<"\n\nPiliahan Anda Tidak Terdaftar !!"<<endl; 
system("pause");
}

}

while(ch!=4); 

return 0;
}

void circular_list::tambah_data() {

n=new node();

n->next = n; n->prev = n;

cout<<"\nMasukan Nama : "; 
getline(cin, n->nama); 
cout<<"Masukan Ttl : "; 
getline(cin,n->ttl);
cout<<"Masukan Jenis Kelamin : "; 
getline(cin, n->jenis_kelamin); 
cout<<"Masukan Alamat : "; 
getline(cin, n->alamat); 
cout<<"Masukan Rt/Rw : "; 
getline(cin, n->rt_rw);
cout<<"Masukan Agama : "; 
getline(cin, n->agama);
cout<<"Masukan Status Perkawinan : "; 
getline(cin, n->status_perkawinan); 
cout<<"Masukan Pekerjaan: "; 
getline(cin, n->pekerjaan); 
cout<<"Masukan Kewarganegaraan : "; 
getline(cin, n->kewarganegaraan); 

if(front==NULL) {
	front=n;

	n->next = front; n->prev = front;

}else{
	rear=front->prev; rear->next = n;
	n->prev = rear; n->next = front; front->prev = n;
}

}

void circular_list::hapus_data() {

string x; temp=front;

if(front==NULL) 
	cout<<"\nCircular Queue Empty!!!"; 
else {

if(front->next!=front) {

x=front->nama; rear = front->prev; front=front->next; rear->next=front;

front->prev= rear; delete temp;

}else{

x=front->nama; front=NULL;
}

cout<<" Data Dengan Nama "<<x<<" Telah Dihapus "<<endl; tampilkan_list();
}

}

void circular_list::tampilkan_list() {

	temp=front; temp1=NULL; if(front==NULL) {

	cout<<"\n\nQueue Dengan Circular Link List Masih Kosong!!!";

}else{
 	cout<<"\n\nIsi Dari Queue Circular Link List:\n\n"; while(temp!=temp1) {

	cout<<"[ "<<temp->nama<<", "<<temp->ttl<<", "<<temp->jenis_kelamin<<", "<<temp->rt_rw<<","<<temp->alamat<<", "<<temp->agama<<", "<<temp->status_perkawinan<<", "<<temp->pekerjaan<<","<<temp->kewarganegaraan<<"]";

if(temp->next!=temp1){ cout<<"-->";
}

temp=temp->next; temp1=front;
}

}

cout<<endl;

}
