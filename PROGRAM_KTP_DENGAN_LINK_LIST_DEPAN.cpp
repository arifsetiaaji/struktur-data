//Program KTP dengan menggunakan link list serta menggunakan operasi tambah dan hapus data dari depan
#include <iostream>
using namespace std;

struct node{
	
	string nama;
	string ttl;
	string jenis_kelamin;
	string alamat;
	string rt_rw;
	string agama;
	string status_perkawinan;
	string pekerjaan;
	string kewarganegaraan;
	node* next; // untuk menghubungkan dengan node lain, tipe data dibuat sama seperi aturan penggunaan pointer.
};

	node* head;
	node* akhir;
	node* cekData;
	node* insert;
	node* del;
	
class Vibe{
	
public:
	void inisialisasi(){
	head = NULL;
	akhir = NULL;
}
void input(string nama, string ttl, string jenis_kelamin, string alamat, string
rt_rw, string agama, string status_perkawinan, string pekerjaan, string
kewarganegaraan){
	insert = new node(); //alokasi memori/ membuat objek
	insert->nama = nama;
	insert->ttl = ttl;
	insert->jenis_kelamin = jenis_kelamin;
	insert->alamat = alamat;
	insert->rt_rw = rt_rw;
	insert->agama = agama;
	insert->status_perkawinan = status_perkawinan;
	insert->pekerjaan = pekerjaan;
	insert->kewarganegaraan = kewarganegaraan;
	insert->next = NULL;

if(head==NULL){
	head = akhir = insert;
}
else{
	insert->next = head;
	head = insert;
}

}
void hapus(){
	string nama;
	string ttl;
	string jenis_kelamin;
	string alamat;
	string rt_rw;
	string agama;
	string status_perkawinan;
	string pekerjaan;
	string kewarganegaraan;

if(head==NULL){
	cout<<"\nlinked list kosong, penghapusan tidak bisa dilakukan"<<endl;
}
else{
	nama = head ->nama;
	ttl = head->ttl;
	jenis_kelamin = head->jenis_kelamin;
	alamat = head->alamat;
	rt_rw = head->rt_rw;
	agama = head->agama;
	status_perkawinan = head->status_perkawinan;
	pekerjaan = head->pekerjaan;
	kewarganegaraan = head->kewarganegaraan;
	
//hapus depan
del = head;
head = head->next;
delete del;
cout<<endl;

cout<<"| D A T A Y A N G D I H A P U S |"<<endl;

cout<<"[ "<<nama<<", "<<ttl<<", "<<jenis_kelamin<<","<<alamat<<", "<<rt_rw<<", "<<agama<<", "<<status_perkawinan<<","<<pekerjaan<<", "<<kewarganegaraan<<" ]"<<endl;
}
}

void cetak(){
	cekData = head;
if(head == NULL)
	cout<<"\ntidak ada data dalam linked list"<<endl;
else{

	cout<<"| D A T A D A L A M L I S T |"<<endl;

while(cekData!=NULL){
	cout<<"[ ";
	cout<<cekData->nama<<", "<<cekData->ttl<<", "<<cekData->jenis_kelamin<<", "<<cekData->alamat<<", "<<cekData->rt_rw<<", "<<cekData->agama<<", "<<cekData->status_perkawinan<<", "<<cekData->pekerjaan<<","<<cekData->kewarganegaraan<<" ]"<<"->";

cekData = cekData->next;
if(cekData != NULL){
	cout<<endl;
}

}
	cout<<"NULL"<<endl;
}
}
void menu(){
	char pilih, ulang;
	string nama;
	string ttl;
	string jenis_kelamin;
	string alamat;
	string rt_rw;
	string agama;
	string status_perkawinan;
	string pekerjaan;
	string kewarganegaraan;
do{
	system("cls");
	cout<<"\n-----------------------"<<endl;
	cout<<"| L I N K E D L I S T | "<<endl;
	cout<<"-----------------------"<<endl;
	
	cout<<"Menu : "<<endl;
	cout<<"1. Input data"<<endl;
	cout<<"2. Hapus data"<<endl;
	cout<<"3. Cetak Data"<<endl;
	cout<<"4. Exit"<<endl<<endl;
	cout<<"Masukkan pilihan Anda : ";
	cin>>pilih;
	cin.ignore();
	cout<<endl;

switch(pilih){
	case '1' :
		cout<<" Masukkan nama : ";
		getline(cin, nama);
		cout<<" Masukan tempat tanggal lahir : ";
		getline(cin, ttl);
		cout<<" Masukan jenis kelamin : ";
		getline(cin, jenis_kelamin);
		cout<<" Masukan alamat : ";
		getline(cin, alamat);
		cout<<" Masukan rt/rw : ";
		getline(cin, rt_rw);
		cout<<" Masukan agama : ";
		getline(cin, agama);
		cout<<" Masukan status perkawinan : ";
		getline(cin, status_perkawinan);
		cout<<" Masukan pekerjaan : ";
		getline(cin, pekerjaan);
		cout<<" Masukan kewarganegaraan : ";
		getline(cin, kewarganegaraan);
		input(nama,ttl,jenis_kelamin,alamat,rt_rw,agama,status_perkawinan,pekerjaan,kewarganegaraan);
		break;

	case '2' :
		hapus();
		break;

	case '3' :
		cetak();
		break;

	case '4' :
		exit(0);
		break;

default :
	cout<<"\nPilih ulang"<<endl;
}
	cout<<"\nKembali ke menu?(y/n)";
	cin>>ulang;
}

while(ulang=='y' || ulang=='Y');
}

};

int main(){
	Vibe *obj = new Vibe();
	obj->inisialisasi();
	obj->menu();
	delete obj;
	
	return 0;
}
