//Program KTP dengan menggunakan circular link list
#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;

//* Node Declaration
struct node{
	string nama;
	string ttl;
	string jenis_kelamin;
	string alamat;
	string rt_rw;
	string agama;
	string status_perkawinan;
	string pekerjaan;
	string kewarganegaraan;

struct node *next;
}*last;

//* Class Declaration
class circular_list{
	
public:

	void buat_node(string nama,string ttl,string jenis_kelamin,string alamat,string rt_rw,string agama,string status_perkawinan,string pekerjaan,string kewarganegaraan);
	void insert_depan(string nama,string ttl,string jenis_kelamin,string alamat,string rt_rw,string agama,string status_perkawinan,string pekerjaan,string kewarganegaraan);
	void hapus_node(string nama);
	void tampilkan_list();

circular_llist(){
last = NULL;

	}
};

/*
* Main :contains menu
*/

int main(){
	string nama;
	string ttl;
	string jenis_kelamin;
	string alamat;
	string rt_rw;
	string agama;
	string status_perkawinan;
	string pekerjaan;
	string kewarganegaraan;
	char choice,lanjut;

circular_list cl;
while (1){

cout<<" CIRCULAR LINK LIST "<<endl;

cout<<" 1.Buat Node "<<endl;
cout<<" 2.Tambah Node didepan "<<endl;
cout<<" 3.Hapus Node "<<endl;
cout<<" 4.Cetak Node "<<endl;
cout<<" 5.Keluar "<<endl;

cout<<"Masukan Pilihan : ";
cin>>choice;
cout<<endl;
cin.ignore();

switch(choice){
case '1':
	cout<<" Masukan Nama : ";
	getline(cin, nama);
	cout<<" Masukan ttl : ";
	getline(cin, ttl);
	cout<<" Masukan Jenis Kelamin : ";
	getline(cin, jenis_kelamin);
	cout<<" Masukan Alamat : ";
	getline(cin, alamat);
	cout<<" masukan Rt/Rw : ";
	getline(cin, rt_rw);
	cout<<" Masukan Agama : ";
	getline(cin, agama);
	cout<<" Masukan Status Perkawinan : ";
	getline(cin, status_perkawinan);
	cout<<" Masukan Pekerjaan : ";
	getline(cin, pekerjaan);
	cout<<" Masukan Kewarganegaraan : ";
	getline(cin, kewarganegaraan);
	
cl.buat_node(nama,ttl,jenis_kelamin,alamat,rt_rw,agama,status_perkawinan,pekerjaan,kewarganegaraan);
cout<<endl;
cout<<"\nlanjut ke menu (Y/N) : ";
cin>>lanjut;
if(lanjut == 'y' || lanjut == 'Y'){
system("cls");
}else{
exit(1);
}
break;

case '2':
	cout<<" Masukan Nama : ";
	getline(cin, nama);
	cout<<" masukan Ttl : ";
	getline(cin, ttl);
	cout<<" Masukan Jenis Kelamin : ";
	getline(cin, jenis_kelamin);
	cout<<" Masukan Alamat : ";
	getline(cin, alamat);
	cout<<" Masukan Rt/Rw : ";
	getline(cin, rt_rw);
	cout<<" Masukan Agama : ";
	getline(cin, agama);
	cout<<" Masukan Status Perkawinan : ";
	getline(cin, status_perkawinan);
	cout<<" Masukan Pekerjaan : ";
	getline(cin, pekerjaan);
	cout<<" Masukan Kewarganegaraan : ";
	getline(cin, kewarganegaraan);

cl.insert_depan(nama,ttl,jenis_kelamin,alamat,rt_rw,agama,status_perkawinan,pekerjaan,kewarganegaraan);
cout<<endl;
cout<<"\nlanjut ke menu (Y/N) : ";
cin>>lanjut;
if(lanjut == 'y' || lanjut == 'Y'){
system("cls");
}else{
exit(1);
}
break;

case '3':

if (last == NULL)
{

cout<<"| List masih kosong |"<<endl;

break;
}
cout<<"Masukan nama data yang ingin dihapus: ";
cin>>nama;
cl.hapus_node(nama);
cout<<endl;
cout<<"\nlanjut ke menu (Y/N) : ";
cin>>lanjut;
if(lanjut == 'y' || lanjut == 'Y'){
system("cls");
}else{
exit(1);
}

break;

case '4':

cl.tampilkan_list();
cout<<"\nlanjut ke menu (Y/N) : ";
cin>>lanjut;
if(lanjut == 'y' || lanjut == 'Y'){
system("cls");
}else{
exit(1);

}

break;

case '5':
exit(1);
break;
default:

cout<<" Pilihan anda tidak terdaftar "<<endl;

}
}
return 0;
}
/*
* buat Circular Link List
*/
	void circular_list::buat_node(string nama,string ttl,string jenis_kelamin,string alamat,string rt_rw,string agama,string status_perkawinan,string pekerjaan,string kewarganegaraan)
{

struct node *temp;
temp = new(struct node);
temp->nama = nama;
temp->ttl = ttl;
temp->jenis_kelamin = jenis_kelamin;
temp->alamat = alamat;
temp->rt_rw = rt_rw;
temp->agama = agama;
temp->status_perkawinan = status_perkawinan;
temp->pekerjaan = pekerjaan;
temp->kewarganegaraan = kewarganegaraan;
if (last == NULL)
{
last = temp;
temp->next = last;
}
else
{
temp->next = last->next;
last->next = temp;
last = temp;

}
}
/*
* insert node didepan
*/
void circular_list::insert_depan(string nama,string ttl,string jenis_kelamin,string
alamat,string rt_rw,string agama,string status_perkawinan,string pekerjaan,string
kewarganegaraan)
{
if (last == NULL)
{

cout<<" Penambahan data tidak berhasil, anda harus membuat node terlebih dahulu! "<<endl;

return;
}
struct node *temp;
temp = new(struct node);
temp->nama = nama;
temp->ttl = ttl;
temp->jenis_kelamin = jenis_kelamin;
temp->alamat = alamat;
temp->rt_rw = rt_rw;
temp->agama = agama;
temp->status_perkawinan = status_perkawinan;
temp->pekerjaan = pekerjaan;
temp->kewarganegaraan = kewarganegaraan;
temp->next = last->next;
last->next = temp;
}
/*
* hapus data list
*/
void circular_list::hapus_node(string nama)
{

struct node *temp, *s;
s = last->next;
/* jika hanya ada 1 data list*/
if (last->next == last && last->nama == nama)
{
temp = last;
last = NULL;
free(temp);
cout<<" data dengan nama "<<nama<<" berhasil dihapus dalam list "<<endl;
return;
}
if (s->nama == nama) /*jika hapus data depan/pertama*/
{
temp = s;
last->next = s->next;
free(temp);
cout<<" data dengan nama "<<nama<<" berhasil dihapus dalam list "<<endl;
return;
}
while (s->next != last)
{
/*hapus data diantara/tengah*/
if (s->next->nama == nama)
{
temp = s->next;
s->next = temp->next;
free(temp);
cout<<" data dengan nama "<<nama<<" berhasil dihapus dalam list "<<endl;
return;
}
s = s->next;
}
/*hapus data akhir*/
if (s->next->nama == nama)
{
temp = s->next;
s->next = last->next;
free(temp);
last = s;
cout<<" data dengan nama "<<nama<<" berhasil dihapus dalam list "<<endl;
return;
}

cout<<" data dengan nama "<<nama<<" tidak terdaftar di list "<<endl;
}

/*
* cetak Circular Link List
*/
void circular_list::tampilkan_list()
{
struct node *s;
if (last == NULL)
{

cout<<"| List masih kosong |"<<endl;

return;
}
s = last->next;

cout<<" C I R C U L A R L I N K L I S T "<<endl;

while (s != last)
{
cout<<"[ ";
cout<<s->nama<<", "<<s->ttl<<", "<<s->jenis_kelamin<<", "<<s->alamat<<", "<<s->rt_rw<<", "<<s->agama<<", "<<s->status_perkawinan<<", "<<s->pekerjaan<<", "<<s->kewarganegaraan;
cout<<" ]"<<"=>";
s = s->next;

}
cout<<"[ ";
cout<<s->nama<<", "<<s->ttl<<", "<<s->jenis_kelamin<<", "<<s->alamat<<", "<<s->rt_rw<<", "<<s->agama<<", "<<s->status_perkawinan<<", "<<s->pekerjaan<<", "<<s->kewarganegaraan;
cout<<" ]"<<"=>"<<endl;
}
